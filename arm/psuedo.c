#include <stdio.h>

#define LINESIZE 64;
#define STACKTOP 1024*4;

int main () {
    unsigned int currentPageIndex = 0;
    unsigned int totalMem = 1024*1024*1024;
    unsigned int pageSize = 1024*LINESIZE;
    unsigned int pageTablePageSize = 1024*4;
    unsigned int p1MemLoc = STACKTOP;
    unsigned int p2MemLoc = STACKTOP + pageTablePageSize;
    unsigned int p2MemLimit;

    while (currentPageIndex < totalMem) {
        //M[p1MemLoc] = p2MemLoc;
        printf("M[%u] = %u\n", p1MemLoc, p2MemLoc);
        printf("\n");
        p2MemLimit = p2MemLoc + pageTablePageSize;
        while (p2MemLoc < p2MemLimit && currentPageIndex < totalMem) {
            //M[p2MemLoc] = currenPageIndex;
            printf("M[%u] = %u\n", p2MemLoc, currentPageIndex);
            currentPageIndex += pageSize;
            p2MemLoc += LINESIZE;
        }
        p1MemLoc += LINESIZE;
    }
}
